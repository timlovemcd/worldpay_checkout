# WorldPay Checkout companion plugin
## How to integrate the plugin?
- This plugin is not published on pub.dev. It is avaialable as a public git repository.
- So to integrate it into the flutter app project, one need to provide git info.
- For further information, Please refer the `example` project from `worldpay_checkout` directory.


## How to use the plugin?
- Please refer the `example` app from `worldpay_checkout` directory.
- Don't forget to replace `YOUR_MERCHANT_ID` with your actual merchant id before running the example app.